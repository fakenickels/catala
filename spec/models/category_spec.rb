require 'spec_helper'

describe Category do
	before(:each) do
  		@cat = Category.create! name: 'restaurante'
	end

	context "slug" do
		it 'should generate a slug' do
			@cat.slug.should == 'restaurante'
		end
	end
end
