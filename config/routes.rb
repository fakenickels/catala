Catala::Application.routes.draw do
  devise_for :users, controller: 'registrations'
  
  get 'products/search'
  get 'pag_notifications/create'
  get 'payments/new' => 'payments#new'
  get 'city/get_by_state/:state_id', to: 'city#get_by_state'
  get 'image_uploader' => 'image_uploader#upload' 

 
  resources :categories do 
    resources :ads do
      
      resources :products do
        member do 
          get 'search'
        end
      end

      resources :payments
    end
  end

  root 'welcome#index'
end
