# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

puts 'Creating basic categories'
categories = Category.create(
	[
		{ name: 'Gráfica' }, 
		{ name: 'Dentista' },
		{ name: 'Advogado' },
		{ name: 'Lanches/Alimentação' },
		{ name: 'Informática' }
	]
)

puts 'Creating Admin'
admin = User.create(
		name: 'Gabriel Rubens', 
		email: 'grsabreu@gmail.com',
		cnpj: '06036695306',
		password: 'eim2tcat', 
		password_confirmation: 'eim2tcat',
		admin: true
)

json_states_cities = JSON.parse( File.open(File.dirname(__FILE__) + '/estados-cidades.json').read )

puts 'Creating States and Cities'
json_states_cities['estados'].each do |s|
	state = State.create( name: s['nome'], acronym: s['sigla'] )
	puts "#{s['nome']} created, creating its cities (#{s['cidades'].size})"

	s['cidades'].each do |city|
		City.create( name: city, state_id: state.id )
	end
end

puts 'Setting the address for Admin'

piaui = State.find_by_acronym('PI')
aguabranca = City.where(state_id: piaui.id, name:'Água Branca').first
address = Address.create(
	street: 'Av. Boa Esperança, 182',
	neighbourhood: 'Compasa',
	zipcode: '64460000',
	phone_number: '99332835',
	phone_area_code: '86',
	user_id: 1,
	state_id: piaui.id,
	city_id: aguabranca.id
)

