# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140214123325) do

  create_table "addresses", force: true do |t|
    t.string   "street"
    t.integer  "city_id"
    t.integer  "state_id"
    t.integer  "user_id"
    t.integer  "ad_id"
    t.string   "zipcode",         limit: 8
    t.string   "neighbourhood",   limit: 40
    t.string   "complement"
    t.string   "phone_number",    limit: 40
    t.string   "phone_area_code", limit: 40
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "addresses", ["ad_id"], name: "index_addresses_on_ad_id", using: :btree
  add_index "addresses", ["city_id"], name: "index_addresses_on_city_id", using: :btree
  add_index "addresses", ["state_id"], name: "index_addresses_on_state_id", using: :btree
  add_index "addresses", ["user_id"], name: "index_addresses_on_user_id", using: :btree

  create_table "ads", force: true do |t|
    t.string   "name"
    t.string   "slogan"
    t.string   "address"
    t.text     "description"
    t.string   "site_url"
    t.integer  "likes"
    t.boolean  "hidden",       default: true
    t.date     "expires_at"
    t.string   "slug"
    t.string   "twitter_url"
    t.string   "facebook_url"
    t.integer  "category_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
    t.string   "logo_url"
  end

  add_index "ads", ["category_id"], name: "index_ads_on_category_id", using: :btree
  add_index "ads", ["user_id"], name: "index_ads_on_user_id", using: :btree

  create_table "categories", force: true do |t|
    t.string   "name"
    t.string   "slug"
    t.string   "desc"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "cities", force: true do |t|
    t.string   "name"
    t.integer  "state_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "cities", ["state_id"], name: "index_cities_on_state_id", using: :btree

  create_table "images", force: true do |t|
    t.string   "img_url"
    t.integer  "ad_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "images", ["ad_id"], name: "index_images_on_ad_id", using: :btree

  create_table "payments", force: true do |t|
    t.float    "amount"
    t.integer  "paid_months"
    t.boolean  "confirmed",   default: false
    t.integer  "user_id"
    t.integer  "ad_id"
    t.integer  "status"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "expires_at"
  end

  add_index "payments", ["ad_id"], name: "index_payments_on_ad_id", using: :btree
  add_index "payments", ["user_id"], name: "index_payments_on_user_id", using: :btree

  create_table "products", force: true do |t|
    t.string   "name"
    t.float    "price"
    t.float    "off"
    t.string   "description"
    t.integer  "ad_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "products", ["ad_id"], name: "index_products_on_ad_id", using: :btree

  create_table "states", force: true do |t|
    t.string   "name"
    t.string   "acronym"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "email",                             default: "", null: false
    t.string   "encrypted_password",                default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                     default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name",                   limit: 50
    t.string   "cnpj",                   limit: 15
    t.boolean  "admin"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
