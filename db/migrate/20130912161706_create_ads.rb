class CreateAds < ActiveRecord::Migration
  def change
    create_table :ads do |t|
      t.string  :name
      t.string  :slogan
      t.string  :address
      t.text    :description
      t.string  :site_url
      t.integer :likes
      t.boolean :hidden
      t.date :expires_at
      t.string :slug, unique: true
      t.string  :twitter_url
      t.string  :facebook_url
      t.references :category, index: true
      t.timestamps
    end
  end
end
