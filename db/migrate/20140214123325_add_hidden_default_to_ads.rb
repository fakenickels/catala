class AddHiddenDefaultToAds < ActiveRecord::Migration
  def change
  	change_column :ads, :hidden, :boolean, default: true
  end
end
