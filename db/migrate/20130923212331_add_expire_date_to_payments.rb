class AddExpireDateToPayments < ActiveRecord::Migration
  def change
    add_column :payments, :expires_at, :datetime
  end
end
