class CreateAddresses < ActiveRecord::Migration
  def change
    create_table :addresses do |t|
      t.string :street
      t.references :city, index: true
      t.references :state, index: true
      t.references :user, index: true
      t.references :ad, index: true
      t.string :zipcode, limit: 8
      t.string :neighbourhood, limit: 40
      t.string :complement
      t.string :phone_number, limit: 40
      t.string :phone_area_code, limit: 40

      t.timestamps
    end
  end
end
