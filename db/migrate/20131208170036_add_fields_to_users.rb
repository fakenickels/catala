class AddFieldsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :name, :string, limit: 50
    add_column :users, :cnpj, :string, limit: 15
    add_column :users, :admin, :boolean
  end
end
