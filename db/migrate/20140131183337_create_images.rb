class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.string :img_url
      t.references :ad, index: true

      t.timestamps
    end
  end
end
