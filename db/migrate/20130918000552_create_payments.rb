class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.float :amount
      t.integer :paid_months
      t.boolean :confirmed, default: false
      t.references :user, index: true
      t.references :ad, index: true
      t.integer :status

      t.timestamps
    end
  end
end
