class AddLogoUrlToAds < ActiveRecord::Migration
  def change
    add_column :ads, :logo_url, :string
  end
end
