class Ad < ActiveRecord::Base
    belongs_to :category
    belongs_to :user

    has_many :payments
    has_many :products

    has_one :address
    accepts_nested_attributes_for :address, allow_destroy: true
    
    validates :name, presence: true, length: { minimum: 1 }
    validates :description, presence: true, length: { minimum: 10 }

    PRICE_PER_MONTH = 30

    extend FriendlyId
    friendly_id :name, use: :slugged

    private
	    def should_generate_new_friendly_id?
	    	new_record?
	    end    
end
