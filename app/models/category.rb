class Category < ActiveRecord::Base
    has_many :ads
    
    validates :name, :presence => true, :length => { :minimum => 5 }

    extend FriendlyId
    friendly_id :name, use: :slugged

    private
	    def should_generate_new_friendly_id?
	    	new_record?
	    end
end
