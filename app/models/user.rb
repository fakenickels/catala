class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
         
  has_many :ads
  has_many :payments
  
  has_one :address
  accepts_nested_attributes_for :address,  allow_destroy: true
end
