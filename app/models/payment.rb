class Payment < ActiveRecord::Base
  belongs_to :user
  belongs_to :ad
  
  validates :paid_months, presence: true
end
