class ImageUploaderController < ApplicationController
	before_filter :is_user?, only: [:upload]

	def upload 
		ad = Ad.friendly.find(params[:ad_id])
		logo_url = params[:image_id]

		if admin_or_owner?(Ad.find(ad_id))
			ad.update logo_url: logo_url
		end
	end


	private
	    def is_user?
	        redirect_to '/', notice: 'Você não está logado' unless authenticate_user!
	    end
end
