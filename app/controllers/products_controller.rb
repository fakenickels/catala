class ProductsController < ApplicationController
  before_filter :is_user?, only: [:update, :edit, :destroy, :new]
  # TODO: ProductsController#index will respond a JSON, this will be used in Search   

  def index
  end

  def new
  end

  def show
  end

  def search
    @products = Product.order(:name).where('LOWER(name) LIKE LOWER(?)', "%#{search_params[:name]}%").pluck(:id, :name)
    
    render json: @products    
  end


  private 
  	def search_params
  		params.permit(:name)
  	end

  	def product_params
  		params.require(:product).permit(:name, :price, :off, :description, :ad_id)
  	end

    def is_user?
        redirect_to '/', notice: 'Você não está logado' unless authenticate_user!
    end    
end
