class PaymentsController < ApplicationController
    before_filter :is_logged?, only: [:payments_history]
    include PaymentsHelper
    
    def new
        @user = User.find(current_user.id)
        @ad = Ad.find_by_slug(params[:ad_id])
        @payment = @user.payments.build
    end
    
    def create
        @user = current_user
        @ad = Ad.friendly.find(params[:ad_id])
        @payment = @ad.payments.build( payment_params )
        @payment.user_id = @user.id
        
        # Calculating amount
        paid_months = payment_params[:paid_months].to_i
        amount = price_per_month * paid_months
        @payment.amount = amount

        if @payment.save
            
            @paying = PagSeguro::PaymentRequest.new
            @paying.reference = @payment.id
            @paying.notification_url = redirect_url + '/pag_notifications/create'
            @paying.redirect_url = redirect_url + category_ad_payment_path( @ad.category, @ad, @payment )

            @paying.items << {
                id: @payment.id,
                description: "Usuário (#{@user.name}) ID#{@user.id} pagou por #{paid_months*30} dias",
                amount: amount
            }
            
            @paying.sender = {
                name: @user.name,
                email: @user.email,
                phone: { 
                    area_code: @user.address.phone_area_code,
                    number: @user.address.phone_number 
                },
                cpf: @user.cnpj
            }
            
            @response = response = @paying.register
            
            if response.errors.any?
                raise = response.errors.join('\n')
            else
                @payment.status = 0 # chaging to status #0 - 'waiting confirmation'
                @user.save
                @payment.save
                
                redirect_to response.url
            end
        end
    end
    
    def show
    end
    
    def payment_progress
        
    end
    
    private 
        def payment_params
            params.require(:payment).permit(:paid_months, :user_id, :ad_id)
        end
        
        def is_logged?
            redirect_to '/', notice: 'Você não é um admin' unless current_user.try(:admin?)
        end
end
