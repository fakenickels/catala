class ApplicationController < ActionController::Base    
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_filter :set_permitted_params, if: :devise_controller?

protected
  def set_permitted_params
    devise_parameter_sanitizer.for(:sign_up) do |u| 
    	u.permit(:name, :email, :cnpj, :address, :password, :password_confirmation)
    end

    devise_parameter_sanitizer.for(:sign_in) do |u|
    	u.permit(:email, :password)
    end

    devise_parameter_sanitizer.for(:account_update) do |u| 
    	address = [:id, :street, :zipcode, :neighbourhood, :complement, :phone_area_code, :phone_number, :state_id, :city_id]
    	u.permit(:name, :email, :password, :cnpj, :password_confirmation, :current_password, address_attributes: address)
    end
  end
end
