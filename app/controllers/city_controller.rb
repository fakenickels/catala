class CityController < ApplicationController
  def get_by_state
  	state_id = params[:state_id]
  	cities = City.all.where(state_id: state_id)

  	render json: cities
  end
end
