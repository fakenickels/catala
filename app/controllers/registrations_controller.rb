#FIXME: DeviseController it's ignoring this override

class RegistrationsController < Devise::RegistrationsController
  def update_sanitized_params
    devise_parameter_sanitizer.for(:sign_up) do |u| 
    	u.permit(:name, :email, :cnpj, :address, :password, :password_confirmation)
    end

    devise_parameter_sanitizer.for(:sign_in) do |u|
    	u.permit(:email, :password)
    end

    devise_parameter_sanitizer.for(:account_update) do |u| 
    	u.permit(:name, :email, :password, :cnpj, :address, :password_confirmation, :current_password)
    end
  end


  def account_update_params
    allow = [:name, :email, :password, :cnpj, :address_attributes, :password_confirmation, :current_password]
  end
end