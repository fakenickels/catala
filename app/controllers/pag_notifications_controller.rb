class PagNotificationsController < ApplicationController
  skip_before_filter :verify_authenticity_token, :only => :create
  include PaymentsHelper

  # TODO see if this really works
  def create
      unless request.post?
        redirect_to '/' unless request.post?
      else
        transaction = PagSeguro::Transaction.find_by_code(params[:notificationCode])
        
        if transaction.errors.empty? && transaction.status.approved?
          @payment = Payment.find( transaction.reference )
          
          months = (@payment.amount/price_per_month).months       
          @payment.update expires_at: Time.now + months
          @payment.update confirmed: true, status: 1
        end
        
        render nothing: true, status: 200
      end
  end

end
