class WelcomeController < ApplicationController
  def index
  	unless user_signed_in?
  		render layout: false
  	else
  		redirect_to '/users/edit'
  	end
  end
end
