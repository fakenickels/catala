class AdsController < ApplicationController
  before_action :set_ad, only: [:show, :edit, :update, :destroy]
  before_filter :is_user?, only: [:update, :edit, :destroy, :new]

  include AdsHelper
  include ProductsHelper
  
  # GET /ads
  # GET /ads.json
  def index
    @ads ||= Ad.all.where(hidden: false)
  end

  # GET /ads/1
  # GET /ads/1.json
  def show
  end

  # GET /ads/new
  def new
    @category = Category.friendly.find( params[:category_id] )
    @ad = @category.ads.build
  end

  # GET /ads/1/edit
  def edit
      @category = Category.friendly.find( params[:category_id] )
  end

  # POST /ads
  # POST /ads.json
  def create
    @category = Category.friendly.find( params[:category_id] )
    # TODO: clear :expires_at field here
    @ad = @category.ads.build( ad_params )
    
    if current_user.admin? 
      @ad.hidden = false
    end

    if request.xhr?
      if @ad.save
        render text: @ad.slug
      else
        render text: 'error'
      end
    else
      respond_to do |format|
        if @ad.save
          format.html { redirect_to ad_url( @ad ), notice: 'Seu anúncio foi criado com sucesso' }
          format.json { render action: 'show', status: :created, location: @ad }
        else
          format.html { render action: 'new' }
          format.json { render json: @ad.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # PATCH/PUT /ads/1
  # PATCH/PUT /ads/1.json
  def update
    respond_to do |format|
      if @ad.update(ad_params)
        format.html { redirect_to @ad, notice: 'Anúncio atualizado com sucesso.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @ad.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /ads/1
  # DELETE /ads/1.json
  def destroy
    @ad.destroy
    respond_to do |format|
      format.html { redirect_to '/' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ad
      @ad = Ad.find_by slug: params[:id]
    end

    # Never trust parameters from the scary internet, only allow the white list throug
    def ad_params
        address = [:id, :street, :zipcode, :neighbourhood, :complement, :phone_area_code, :phone_number, :state_id, :city_id]
        params.require(:ad).permit(
          :expires_at, :name, :slogan, :logo_url,:site_url,
          :state, :city, :district, :cep, :tel, :description, 
          :category_id, :user_id, :products, address_attributes: address
        )
    end
    
    def is_user?
        redirect_to '/', notice: 'Você não está logado' unless authenticate_user!
    end
end
