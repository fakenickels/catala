window.City = {
	get_by_state: function( stateID, callback ){
		$.get('/city/get_by_state/' + stateID, function( cities ){ 
			callback( cities )
		});	
	}
}