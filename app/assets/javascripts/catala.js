+function($){
	
	/* Funcoes uteis */
	var fn = {
	/*  Recebe texto, e indice
		Retorna o texto que vem antes do indice */
		eachLetter: function(text, i){
			return text.substr(0,i);
		},
	/* Anula o loop do autoText, se textClean positivo o val do input fica vazio */
		clean: function( nodes , textClean){
			clearInterval(nodes.data('loop'));
			nodes.removeData('loop');
			if( textClean )
				nodes.val('');
			return nodes;
		}
	}


	/* Metodo do jQuery */

	$.fn.autoText = function( text, callback ){

		/* Argumentos this, a barra que fica piscando, e o indice*/
		var self = this,
			bar = '',
			i = 0;

		/* Desativa o autoText */
		if( text === false ){
			fn.clean( self , true);
			return self;
		}

		/* Salva nestes nodes a funcao loop do interval */
		self.data('loop', setInterval(function(){
			/* Se o indice for par, coloca a barra */
			bar =  (i % 2 == 0 || i != text.length ) ? '|' : '';
			/* Enquanto o indixe menor doque o texto */
			if( i <= text.length ) //Atribue o valor para o input
				self.val( fn.eachLetter(text, i++) + bar ); 
			else {
				/* Quando a frase tiver terminado, limpa e chama callback */
				fn.clean( self );
				setTimeout(function(){
					self.val('');
					if( callback) callback.call(self);
				}, 800);
			}
		}, 100));

		return self;	
	};

	/* Desliga o autoText */
	$.fn.offText = function(){
		return $(this).autoText(false);
	};



	$.fn.autoComplete = function( words, opts ){
		
		var self = this,
			result = [],
			keyWords = opts.output.match(/\$\w+/g);

		
		function getNext( index ){
			var last;
			for( var i = index; i >= 0; i--){
				if( i != 0 ){
					$.each( words[keyWords[i - 1]], function(){
						//Code
					});
				}
				last = words[keyWords[i]];
			}
		}
		getNext( keyWords.length - 1 );

	}

}(jQuery);

Catala = {
	nodes:{},
	validate: function( searchStr ){
		var categories = ['restaurantes', 'hoteis', 'lanchonetes','butecos','cabare','farmacia', 'grafica'],
			products = ['sanduiches', 'arrumadinhos', 'quartos'],
			searchOptions = {
				category: '',
				local: '',
				product: ''
			},
			getNextLocal = false;

		searchStr = searchStr.toLowerCase();

		$.each( categories, function( i, cat ){
			var found = searchStr.match( cat );

			if( found ){
				searchOptions.category = cat;
			}
		});	

		$.each( products, function( i, prod ){
			var found = searchStr.match( prod );

			if( found ){
				searchOptions.product = prod;
			}
		});	

		var city = searchStr.split('em');
		if( city[1] ){
			searchOptions.local = $.trim( city[1] ); 
		}

		// $.each(searchStr, function( i, word ){
		// 	if( categories.indexOf(word) > -1 ){
		// 		searchOptions.category.push(word);
		// 	}
		// 	if( word == 'em' ){
		// 		getNextLocal = true;
		// 		return;
		// 	}
		// 	if( getNextLocal ){
		// 		searchOptions.local.push(word);
		// 		getNextLocal = false;
		// 	}
		// });

		console.log( searchOptions );
	},
	request: function(){
		
	},
	init: function(){
		
		var self = this,
			nodes = this.nodes;

		nodes.welcomeSearch = $('#welcome .search .input-search');
		nodes.welcomeBtn = $('#welcome .search .btn-search');

		(nodes.welcomeSearch)
			.autoText('Restaurantes em Água Branca', function(){
				$(this).autoText('Quartos em Teresina', function(){
					$(this).popover('show');
				});
			})
			.click(nodes.welcomeSearch.offText)
			.focus(nodes.welcomeSearch.offText);

		(nodes.welcomeBtn)
			.click(function(){

				self.validate( nodes.welcomeSearch.val().toLowerCase() );
			});

	},

	login: function(){

	}
}