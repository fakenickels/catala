$(function(){
window.Modal = {
	clear: function(){
		return Modal.header('').body('').footer('');
	},

	resourceView: function( options ){
		Modal
			.header( options.title )
			.body(tmpl('data-listing-tmpl', options), 'html')
			.footer('')
			.show();
	},

	show: function(){
		// Redundance heuahsahs
		Modal.modal.modal('show');

		return Modal;
	},

	hide: function(){
		Modal.modal.modal('hide');

		return Modal;
	},

	/* Set a complete modal
	*
	*	set( options = { 
	*		header: '...'|jQueryElement, 
	*		body: '...'|jQueryElement, 
	*		footer: '...'|jQueryElement 
	*	});
	* @object options
	*/
	set: function( options ){
		Modal
			.header.apply( this, options.header )
			.body.apply( this, options.body )
			.footer.apply( this, options.footer );

		return Modal;
	},

	/* Set the modal header|body|footer
	*
	*	i.e header( text, [html] )
	*	@string html says to render text as html
	*/
	header: function( text, html ){
		Modal.modal.find('#myModalLabel')[ html ? 'html' : 'text' ]( text );
		return Modal;
	},
	
	body: function( text, html ){
		Modal.modal.find('.modal-body')[ html ? 'html' : 'text' ]( text );
		return Modal;
	},

	footer: function( text, html ){
		Modal.modal.find('.modal-footer')[ html ? 'html' : 'text' ]( text );
		return Modal;
	},

	dialog: {
		alert: function( msg ){
			Modal
			.header('Alerta')
			.body('<h4>'+ msg +'</h4>', 'html')
			.footer('<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>', 'html')
			.show();			
		},

		confirm: function( msg, callback ){
			var yesBtn = $('<button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>'),
				noBtn = $('<button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>');
			Modal
				.header('Confirmar')
				.body('<h4>'+ msg +'</h4>')
				.footer( $([yesBtn, noBtn]), 'html' );

			yesBtn.click(function(){
				callback( true )
			});

			noBtn.click(function(){
				callback( false );
			});
		}
	}				
};

Modal.modal = $('#main-modal');
})