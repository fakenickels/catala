module AdsHelper
    def ad_url(ad)
        "/categories/#{ad.category.slug}/ads/#{ad.slug}"
    end
    
    def ads_path(cat)
        "/categories/#{cat.slug}/ads/"    
    end
    
    def edit_path(ad)
        "/categories/#{ad.category.slug}/ads/#{ad.slug}/edit"
    end
    
    def is_admin_or_owner?(ad)
        if user_signed_in?
           current_user.admin? or ad.user.id == current_user.id
        end
    end

    def is_admin_or_waiting_user?
        if user_signed_in?
            current_user.admin? or current_user.paid_but_not_yet_posted?
        end
    end
end
