module ProductsHelper
	def new_product_url(ad)
		"/categories/#{ad.category.slug}/ads/#{ad.slug}/product/new"
	end
end
