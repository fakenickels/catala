module ApplicationHelper
    def put_if_signed( msg )
        if user_signed_in?
            msg
        end
    end
    
    def put_if_admin( msg )
        if current_user.try(:admin?)
            msg
        end
    end
    
    def is_admin?
        current_user.try(:admin?)
    end

    def admin_or_owner?( ad )
        current_user.admin? || ad.user_id == current_user.id 
    end

    def bAlert(msg, type)
        "<div class='alert alert-#{type}'>#{msg}</div>"
    end
end
