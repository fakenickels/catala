json.array!(@ads) do |ad|
  json.extract! ad, :name, :slogan, :address, :tel, :description, :category_id
  json.url ad_url(ad, format: :json)
end
