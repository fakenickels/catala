require 'test_helper'

class PaymentsControllerTest < ActionController::TestCase
  test "should get amount:float" do
    get :amount:float
    assert_response :success
  end

  test "should get user:references" do
    get :user:references
    assert_response :success
  end

  test "should get months_bought:integer" do
    get :months_bought:integer
    assert_response :success
  end

end
