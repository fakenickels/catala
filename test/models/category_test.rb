require 'test_helper'

class CategoryTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  before(:each) do
  	@category = Category.create name: 'Clínica'
  end

  context(:slug) do
  	it 'should generate a slug' do
  		@category.slug.should == 'clinica'
  	end
  end
end
