namespace :ads do
  desc "Finds expired ads and unvalidate them [set Ad#hidden to true if Ad is expired]"
  task find_expired: :environment do
    puts 'Initializing.'
    
    unless Ad.where( hidden: false ).size == 0
        count = 0
        Ad.all.each do |ad|
            unless ad.payments.empty?
                payment = ad.payments.last
                unless payment.expires_at.nil?
                    pay_day = payment.expires_at
                    if Time.now > pay_day
                        puts "Woow! Ad ##{ad.id} is expired -> Unvalidating it"
                       
                        ad.update hidden: true 
                        ad.payments.last.update status: -1 
                        count += 1

                        puts "Successfully."
                    end
                end
            end
        end
    else
        puts 'Nothing to do.'    
    end
    
    puts 'All done.', "Expired total #{count}"
  end

end
